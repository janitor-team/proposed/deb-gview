/********************************************************************
 *            dvpreview.h
 *
 *  Thu Sep 14 21:37:23 2006
 *  Copyright  2006  Neil Williams
 *  linux@codehelp.co.uk
 *******************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef _DVPREVIEW_H
#define _DVPREVIEW_H

#include <glib.h>
#include "dvarchive.h"

/* Pass the uncompressed content to another application.

param tmp_file The name of a temporary file containing the
	uncompressed content.
param filename The original filename from the archive within
	the package - precisely as the file is inside the package.
*/
gboolean
dv_file_spawn (const gchar * tmp_file, DVContents * deb);

/* initialise the key file */
gboolean 
preview_init (void);

/* shutdown the preview data table */
void
preview_shutdown (void);

void
dv_spawn_external (GtkWidget * widget, DVContents * deb);

gboolean
dv_show_our_manpage (DVContents * deb);

#endif /* _DVPREVIEW_H */
