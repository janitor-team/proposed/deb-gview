2014-08-30  gettextize  <bug-gnu-gettext@gnu.org>

	* m4/gettext.m4: New file, from gettext-0.19.2.
	* m4/iconv.m4: New file, from gettext-0.19.2.
	* m4/lib-ld.m4: New file, from gettext-0.19.2.
	* m4/lib-link.m4: New file, from gettext-0.19.2.
	* m4/lib-prefix.m4: New file, from gettext-0.19.2.
	* m4/nls.m4: New file, from gettext-0.19.2.
	* m4/po.m4: New file, from gettext-0.19.2.
	* m4/progtest.m4: New file, from gettext-0.19.2.
	* Makefile.am (EXTRA_DIST): Add config.rpath.
	* configure.ac (AC_CONFIG_FILES): Add po/Makefile.in.

2012-01-08  Neil Williams  <linux@codehelp.co.uk>

	* configure.in: Version bump.
	* src/main.c: Patch from Andres Mejia <mcitadel@gmail.com>
	  to  allow build with either libarchive (2.8.5) or (3.0.2)

2009-11-03  Neil Williams  <linux@codehelp.co.uk>

	* configure.in: Fix PACKAGE_LIBS mangling.
	Ensure gdk-pixbuf is present.
	* src/Makefile.am: Add -Wl,-z,defs -Wl,--as-needed.

2009-11-01  Neil Williams  <linux@codehelp.co.uk>

	* deb-gview.1.xml: Remove details of bonobo and gnome
	help options removed when gnome support was removed.

2009-10-31  Neil Williams  <linux@codehelp.co.uk>

	* Makefile.am: Generate manpage.
	* configure.in: Bump version and generate a bz2 tarball.
	* deb-gview.1.xml: Document migration to XDG Spec.
	* src/dvpreview.c: Check for old file and move it to
	the new XDG home.

2009-08-20  Neil Williams  <linux@codehelp.co.uk>

	* desktop/deb-gview.desktop.in.in: Drop deprecated Encoding.

2009-08-20  Neil Williams  <linux@codehelp.co.uk>

	* autogen.sh: Add gettext macro symlinks
	* configure.in: Drop libgnome and libgnomeui
	* m4/.cvsignore: ignore gettext macros.
	* src/Makefile.am: Drop GNOME_LIBS
	* src/callbacks.c: No need for libgnome
	* src/callbacks.h: No need for libgnome
	* src/main.c: Replace gnome_program_init with gtk_init
	* website/index.html: New version changes.
	* NEWS: summary.

2009-04-03  Neil Williams  <linux@codehelp.co.uk>

	Makefile.am : refresh m4 support and tidy up
	autosave files.
	configure.in : refreshed autotools support.

2009-03-09  Neil Williams  <linux@codehelp.co.uk>

	* src/callbacks.c (get_deb), src/dvarchive.c (set_deb_file):
	Add support for opening TDebs.

2008-04-13  Neil Williams  <linux@codehelp.co.uk>

	* src/dvarchive.c (dv_parse_changes): Format 1.8 of
	.changes files for dpkg-dev 1.14.18 is not the same as
	Format 1.8 for dpkg-dev 1.14.17 - handle the definition of
	Format 1.8 from dpkg-dev 1.14.18 (Files section follows
	Checksums section, rather than preceding it in 1.14.17.)
	* configure.in : Bump version for next release.

2008-03-22  Neil Williams  <linux@codehelp.co.uk>

	* src/callbacks.c (on_dvtoolopen_clicked), (new_window_init): 
	Pass the context so that the new functions can be used.
	* src/dvarchive.c (dv_set_needs_uri), (dv_get_needs_uri),
	(dv_parse_changes): Support a flag to dictate whether a GFile
	needs URI or path parsers, add routines to get and set the flag.
	* src/dvarchive.h: New get and set functions.
	* src/main.c (convert_vfs_to_gfile): Set the URI flag when
	necessary.* src/dvarchive.c (dv_archive_free): Clear the GFile object
	when closing each window.
	* src/main.c (convert_vfs_to_gfile), (main): Run checks
	on all GFile methods - fails with remote access until
	GVfs allows operation without the need for sudo.

2008-03-21  Neil Williams  <linux@codehelp.co.uk>

	* website/index.html,
	 website/manpage.html: 0.2.0 release.
	* src/callbacks.c (on_open_deb), (on_dvtoolopen_clicked),
	(new_window_init): Integrating with new GFile support.
	* src/dvarchive.c (dv_new_window), (dv_parse_changes),
	(set_deb_file), (dv_archive_open), (dv_archive_read),
	(prepare_contents), (open_deb), (dv_archive_preload):
	Ensure the same stream is always available to keep
	track of the files within the .deb. Migrate .changes
	support to GFile path handling.
	* src/dvarchive.h: Using new GFile support.

	* src/callbacks.c (get_deb), (on_dvtoolopen_clicked),
	(new_window_init), (on_treeview_button_press_event),
	src/dvarchive.c (dv_new_window), (dv_set_working_path)
	src/main.c (convert_vfs_to_gfile) : Restore
	consistent handling of workingdir for New and Open operations.

2008-03-19  Neil Williams  <linux@codehelp.co.uk>

  * configure.in: autoupdate changes, replace libgnomevfs
  with GIO. Bump version.
  * src/callbacks.c (on_dvtoolopen_clicked), (new_window_init),
  src/dvarchive.h, src/main.c (main), 
  src/dvarchive.c (dv_new_window), (dv_parse_changes),
  (set_deb_file), (dv_archive_open), (dv_archive_read),
  (prepare_contents), (open_deb): 
  Migrate from libgnomevfs to GIO.

2008-03-05  Neil Williams  <linux@codehelp.co.uk>

 * autogen.sh,
 configure.in : Replace separate commands with
 autoreconf -ifs.

2007-12-24  Neil Williams <linux@codehelp.co.uk>

	* configure.in: Isolate only the libraries we need from
	the pkg-config lines.
	Bump to 0.1.5
	* src/Makefile.am: Limit the number of libraries linked.

2007-08-25  Neil Williams <linux@codehelp.co.uk>

	* COPYING,
	debian/copyright,
	src/*.c,
	src/*.h : Move to GPL v3.

2007-08-09  Neil Williams <linux@codehelp.co.uk>

	* configure.in : Remove ALL_LINGUAS in preference for
	po/LINGUAS so that new language translations can be added
	without editing configure.in.
	* po/LINGUAS : new file.

2007-07-30  Neil Williams <linux@codehelp.co.uk>

	* configure.in: Bump to version 0.1.3
	* deb-gview.1.xml: Clarify documentation of 
		multi-window usage (Closes: #435071)
	* debian/control: Suggest eog instead of out-of-date qiv 
	* src/callbacks.h
	src/callbacks.c: (on_close_activate): Add close option to menu.
	* src/dvpreview.c: (create_groups): Suggest eog instead of out-of-date qiv 
	* src/interface.c: (create_aboutdialog) : Use the license declaration
	instead of the entire GPL licence.
	* src/interface.c: (create_deb_gview_window): Add tooltips for each
	toolbar button. Awaiting new translations before release.

2007-05-30  Neil Williams <linux@codehelp.co.uk>

	* deb-gview.gladep,
	deb-gview.glade : Removed. With the move to glade3, the glade2 
	xml is no longer necessary - using the generated C from glade2 for
	future development.

2007-05-15  Neil Williams <linux@codehelp.co.uk>

	* configure.in,
	po/cs.po : New Czech translation.
	Miroslav Kure <kurem@upcase.inf.upol.cz>
	Bump to v0.1.2

2007-04-17  Neil Williams <linux@codehelp.co.uk>

	* configure.in,
	po/nl.po : New Dutch translation.
	Bart Cornelis <cobaco@skolelinux.no>
	* src/dvarchive.c : Set the package filename
	in the window title bar.
	* src/callbacks.c : Fix failure to close About
	dialog with Close button. Use gtk_dialog_run instead
	of gtk_widget_show.

2007-04-14  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c : Set the Package column
	as the default sort so that the initial display
	sorts alphabetically by the filenames in the package.
	* src/main.c : Ensure relative paths are handled
	correctly by VFS by creating a full path name.

2007-04-10  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c : Support remote 
	.changes files using GnomeVFS.

2007-04-09  Neil Williams <linux@codehelp.co.uk>

	* debian/control : Add XS-X-Vcs-CVS and 
	XS-X-Vcs-Browser URL's.
	* src/callbacks.c,
	src/dvpreview.c : Update and tweak function
	layout.
	* src/dvarchive.c,
	src/dvarchive.h,
	src/main.c : Implement GnomeVFS support.
	(Remote .changes files are usually deleted after an upload
    and are therefore unsupported in deb-gview.)

2007-04-04  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c,
	src/main.c : Begin migration to gnome-vfs to
	support remote locations.

2007-04-02  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c,
	src/main.c : Include support for opening
	.changes files.

2006-12-30  Neil Williams <linux@codehelp.co.uk>

	* deb-gview.glade,
	po/en_GB.po,
	po/fr.po,
	src/interface.c : Fix translation omission due 
	to glade bug that references the unknown file
	deb-gview.glade.h (See #404974).
	* po/fr.po : New French translation 
	(Closes: 404969).

2006-12-26  Neil Williams <linux@codehelp.co.uk>

	* src/callbacks.c,
	src/dvarchive.c,
	src/dvarchive.h : Fix open dialogue to not
	always default to /var/cache/apt/archives
	(Closes: #390595).

	* deb-gview.1,
	deb-gview.1.xml,
	debian/changelog,
	po/en_GB.po,
	po/fr.po : Updating for 0.0.6 release.

2006-12-14  Neil Williams <linux@codehelp.co.uk>

	* src/callbacks.c : Outline support for .udeb
	archives.
	* website/index.html : Include Ubuntu version.
	* autogen.sh,
	configure.in : Remove maintainer mode.

2006-09-20  Neil Williams <linux@codehelp.co.uk>

	* debian/control : Recommend x-terminal-emulator.
	* src/dvpreview.c : Allow use of customised
	man page viewer, if one becomes available,
	from Help.
	Allow configuration of the command to pass to 
	the chosen terminal.
	* deb-gview.1.xml,
	deb-gview.1,
	website/manpage.html : Document change in
	preview configuration.

2006-09-19  Neil Williams <linux@codehelp.co.uk>

	* NEWS,
	debian/changelog,
	po/ChangeLog : 0.0.5 release.

2006-09-16  Neil Williams <linux@codehelp.co.uk>

	* Makefile.am : Build the pot file before
	changing into the po subdirectory.

	* src/dvpreview.c : Fixing suffix and temporary
	name handling for binary files. g_file_open_tmp
	only creates text based files.

	* src/interface.c,
	deb-gview.glade : Fix popup menu to use the same
	label and shortcut as the main menu version.

2006-09-15  Neil Williams <linux@codehelp.co.uk>

	* deb-gview.1 : Improved layout.
	* deb-gview.glade : Remove unnecessary tooltip.
	* po/en_GB.po : Remove unnecessary messages.
	* src/dvpreview.c : Remove debugging warning.
	* src/interface.c : Remove tooltip.

2006-09-15  Neil Williams <linux@codehelp.co.uk>

	* NEWS : Note that sortable columns now work.
	* README : Tweak.
	* TODO : Record that image previews now supported.
	* configure.in : v0.0.5
	* deb-gview.1 : Document file preview changes.
	* deb-gview.glade : Add popup menu on right click,
	tweak translatable strings, new toolbar button to
	view the manpage and new menu option to go with the
	popup menu to execute an external viewer.
	* debian/control : Tweak and suggest some external
	viewer applications.
	* po/POTFILES.in : New preview source file.
	* po/en_GB.po : New strings.
	* src/Makefile.am : New preview files.
	* src/callbacks.c,
	src/callbacks.h,
	src/dvarchive.c,
	src/dvarchive.h,
	src/dvpreview.c,
	src/dvpreview.h,
	src/interface.c,
	src/interface.h,
	src/main.c :  File preview handling.

2006-09-08  Neil Williams <linux@codehelp.co.uk>

	* configure.in,
	po/fr.po : Added French translation.
	Thomas Huriaux <thomas.huriaux@gmail.com>

2006-09-03  Neil Williams <linux@codehelp.co.uk>

	* configure.in,
	debian/changelog,
	debian/rules,
	po/en_GB.po : Prepare for 0.0.4 release.

	* configure.in : Test for libz.

	* configure.in,
	src/Makefile.am,
	src/dvarchive.c,
	src/dvarchive.h,
	src/main.c : Implementing and using extra
	compiler warnings.

	* src/main.c : Tweak.
	* src/dvarchive.h : Tweak.
	* src/dvarchive.c : Temporary workaround for
	gzipped files using glib temporary files and
	zlib1g. libarchive unable to open .gz, only
	.tar.gz, in memory.

2006-08-31  Neil Williams <linux@codehelp.co.uk>

	* src/callbacks.c,
	src/dvarchive.c,
	src/dvarchive.h,
	src/main.c : Separating functionality of Close
	and Quit so that close only operates on the
	current window, Quit exits all and no window
	is left running.

	* deb-gview.glade,
	src/callbacks.c,
	src/interface.c,
	src/main.c : Remove the second close button.

	* src/callbacks.c,
	src/dvarchive.c,
	src/dvarchive.h : Fix bug in columns when opening
	further packages in the same window.

2006-08-30  Neil Williams <linux@codehelp.co.uk>

	* debian/menu,
	desktop/deb-gview.desktop.in.in : switching 
	to .xpm icon and correcting filename.
	* desktop/dpkg-view.png : removed.
	* desktop/deb-gview.xpm : Added, courtesy of
	Lior Kaplan <kaplan@debian.org>. 
	Debian Bug #385267.

	* src/dvarchive.c,
	src/main.c : fix FSF address.

2006-08-29  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c : Handle an empty .deb file
	and standardise error syntax.
	* src/main.c : Handle empty .deb files on the
	command line.

2006-08-28  Neil Williams <linux@codehelp.co.uk>

	* src/Makefile.am : avoid specifying -larchive twice
	* src/dvarchive.c : Standardise some translatable strings.
	
	* po/en_GB.po : Update for modified strings.

	* website/index.html : Use a specific bug page

2006-08-23  Neil Williams <linux@codehelp.co.uk>

	* configure.in : Incorporate settings from
	autoscan to check for working -larchive.

2006-08-23  Neil Williams <linux@codehelp.co.uk>

	* NEWS : Tidy up.
	* README : Clarify direction.
	* configure.in : correct pixmap location.
	* deb-gview.1 : reflect description in manpage.
	* deb-gview.gladep : glade management of pixmaps.
	* debian/changelog : Make 0.0.3 the initial Debian release.

2006-08-14  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c : Implement status bar, downgrade
	compression errors to status bar warnings. Warn
	about empty files and UTF-8 conversion failures.
	Ensure files are 'regular files' before storing
	content.

	* configure.in : pixmaps location.
	* deb-gview.glade : Status bar and toolbar.
	* desktop/Makefile.am : pixmaps location fix.

	* src/callbacks.c : Add Toolbar support. Add file filter
	support.
	* src/callbacks.h : Toolbar support.
	* src/dvarchive.h : Tweak.
	* src/interface.c : Toolbar and status bar support.
	* src/main.c : Locate pixmap icon for main window
	and handle multiple instances.

	* debian/control : Tweak description.

	* TODO : Tweak for release.
	* po/en_GB.po : New strings added.
	* website/index.html : release update.

2006-08-04  Neil Williams <linux@codehelp.co.uk>

	* Various : Renaming package deb-gview. See
	bug #381221.
	* src/dvarchive.c : clear rows and columns 
	when opening a new file.
	Removing example code and use of temporary 
	files. Fixing data list retrieval.
	* src/callbacks.c : Prevent null files being 
	loaded.

2006-08-02  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.c : Fix column handling when
	opening multiple package files from the one
	window.

	* Makefile.am : Package debian/ directory
	to make dpkg-view into a Debian native package.
	* configure.in : Increment version to reflect 
	change in nature.

2006-08-01  Neil Williams <linux@codehelp.co.uk>

	* src/dvarchive.h :
	* src/dvarchive.c : New files to handle the ar and 
	tar.gz archives.
	* src/interface.c : Enlarge the default window and make
	the list hover sensitive.
	* callbacks.c,
	callbacks.h : Relocating code to dvarchive.c|h
	* src/main.c : Fixing handling of command line files 
	and help options. Remove old comments.
	* po/en_GB.po : update
	* src/callbacks.c,
	src/dvarchive.c,
	src/dvarchive.h,
	src/main.c : Fixing display of text files within package
	files.

2006-07-30  Neil Williams <linux@codehelp.co.uk>

	* Makefile.am,
	 src/Makefile.am
	 src/callbacks.c
	 src/main.c : Building libarchive support 
	 and outputting some content.
	 Removing debug code for those bits that now work.
	
	* desktop/ : Adjusting desktop config.

	* configure.in : Enable stricter error checking.
	
	* main.c : Enable auto-opening of files specified
	on the command line.
	
	* dpkg-view.1 : Manpage created with help2man and
	tweaked.

2006-07-29  Neil Williams <linux@codehelp.co.uk>

	* dpkg-view.glade,
	src/callbacks.c,
	src/callbacks.h,
	src/interface.c,
	src/interface.h,
	src/main.c : Beginning to create usable 
	output

	* various : Initial project creation.
